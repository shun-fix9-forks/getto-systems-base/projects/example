pub const HEADER_NONCE: &'static str = "X-GETTO-EXAMPLE-NONCE";

pub const COOKIE_TICKET_TOKEN: &'static str = "__Secure-GETTO-EXAMPLE-TICKET-TOKEN";
pub const COOKIE_API_TOKEN: &'static str = "__Secure-GETTO-EXAMPLE-API-TOKEN";

pub const COOKIE_CLOUDFRONT_SIGNATURE: &'static str = "CloudFront-Signature";
pub const COOKIE_CLOUDFRONT_KEY_PAIR_ID: &'static str = "CloudFront-Key-Pair-Id";
pub const COOKIE_CLOUDFRONT_POLICY: &'static str = "CloudFront-Policy";
