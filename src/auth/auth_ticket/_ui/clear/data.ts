import { RemoteCommonError } from "../../../../../ui/vendor/getto-application/infra/remote/data"

export type ClearAuthTicketError = ClearAuthTicketRemoteError

export type ClearAuthTicketRemoteError = RemoteCommonError
